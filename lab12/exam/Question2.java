package exam;

public class Question2 {

	private static int max(int a, int b, int c, int d) {
		int max = 0;

		if (max < a)
			max = a;
		if (max < b)
			max = b;
		if (max < c)
			max = c;
		if (max < d)
			max = d;
		return max;
	}

	public static void main(String[] args) {
		System.out.println(max(1, 2, 3, 4));
	}

	
}
