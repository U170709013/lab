package exam;

public class Question3 {

	public static int secondMax(int[] array) {
		int max = array[0];
		int secondMax = array[0];

		for (int i = 1; i < array.length; i++) {
			if (max < array[i]) {
				secondMax = max;
				max = array[i];
			} else if (secondMax == max) {
				secondMax = array[i];
			} else if (secondMax < array[i]) {
				secondMax = array[i];
			}
		}
		if (secondMax == max)
			return -1;
		return secondMax;
	}

	public static void main(String[] args) {
		int[] arr = { 1, 2, 3, 4, 10, 3 };

		System.out.println(secondMax(arr));
	}
}
