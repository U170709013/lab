
public class MyDate {
	
	int day,month,year;
	
	int[] max_days = {31,28,31,30,31,30,31,31,30,31,30,31};
	
	MyDate(int day,int month,int year){
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public void incrementDay() {
		day++;
		if (day > max_days[month - 1]) {
			if(month == 1 && inLeapYear())
				day = 29;
			else {
				day = 1;
				incrementMonth();
			}
		}
	}
	
	public void incrementDay(int i) {
		day+=i;
		
	}
	
	public void decrementDay() {
		day--;
		if(day == 0) {
			decrementMonth();

			if(month == 2 && inLeapYear())
				day = 29;
			else {
				day = max_days[month-1];
			}
		}	
	}
	
	private boolean inLeapYear() {
		return year % 4 == 0;
	}

	public void decrementDay(int i) {
		day-=i;
	
	}
	
	public void incrementYear() {
		year++;
		
	}
	
	public void incrementYear(int i) {
		year += i;
	}
	
	public void decrementYear() {
		year--;
		
	}
	
	public void decrementYear(int i) {
		year-=i;
	
	}
	
	public void incrementMonth() {
		incrementMonth(1);
	}
	
	public void incrementMonth(int i) {
		int newMonth = (month + i) % 12;
		int yearDiff = 0;
		if (newMonth < 0) {
			newMonth += 12;
			yearDiff = -1;
		}
		yearDiff = (month + i) / 12;
		month = newMonth;
		incrementYear(yearDiff);
	}
	
	public void decrementMonth() {
		decrementMonth(1);
	}
	
	public void decrementMonth(int i) {
		incrementMonth(-i);
		
	}

	public boolean isBefore(MyDate anotherDate) {
		// TODO Auto-generated method stub
		return false;
	}

	public int dayDifference(MyDate anotherDate) {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean isAfter(MyDate anotherDate) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public String toString() {
		return ""+year+ "-" + (month > 9 ? "":"0") + month + "-"+ (day > 9 ? "":"0") + day;
	}
	
}
